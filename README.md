Belbakkal Amine \
Rafaele Antonio \
Delbeke Julien \
Ramos Perez Nestor 

The goal of this project was to implement a rainbow attack in order to crack passwords of minimum length 8.
Before compiling the code, you have to download and install the c++ library boost. On Linux you can type this command: sudo apt-get install libboost-all-dev.
In order to compile the code we made a MakeFile. So execute the following command:
   make
   
 This will generate an executable 'Rainbow', to execute it you have to execute the following command:
    ./Rainbow arg1 arg2
  
 In the command arg1 corresponds to an int with the length of the passwords to crack and arg2 corresponds to an int with the number of passwords you want to crack.
   
When you execute the command above, the program generates the table and attacks. To choose whether you want to generate a new table or you want to attack from a known table, you can change in the main.cpp the boolean:

Rainbow *a = new Rainbow(lengthPassword,"Rainbow.csv",false);

false meaning the programs will generate a new table and true it will use the existing 'Rainbow.csv" in the same directory.
