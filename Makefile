All_flag=-O3 -std=c++17 -Wpedantic -Wall -Wextra -Wconversion -Weffc++ -Wstrict-null-sentinel -Wold-style-cast -Wnoexcept -Wctor-dtor-privacy -Woverloaded-virtual -Wsign-promo -Wzero-as-null-pointer-constant -Wsuggest-final-types -Wsuggest-final-methods -Wsuggest-override 

all: Rainbow

Rainbow: main.cpp Rainbow.cpp Rainbow.hpp sha256.cpp sha256.h
	g++ $(All_flag) -pthread main.cpp Rainbow.cpp sha256.cpp -o Rainbow

clean: 
	rm Rainbow	
