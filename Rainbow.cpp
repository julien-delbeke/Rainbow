#include <iostream>
#include "sha256.h"
#include <fstream>
#include <cstring>
#include <stdlib.h>
#include <math.h>
#include "Rainbow.hpp"
#include <vector>
#include <random>
#include <boost/algorithm/string.hpp>
#include <thread>


Rainbow::Rainbow(int wordlength,string fileName,bool fileExist): _iteration(REDUCTIONS),_wordlength(wordlength),_allLetters(ALLKEY),_fileName(fileName){
	if(not fileExist){
		generateTable();
	}
}


void Rainbow::reduceAndHashingProcess(int indexBigList,int index,std::vector<std::vector<std::string>>* information){
	/*Function executing the : 
	- Password creation
	- Process of hashing and reducing the password.*/
	string passwordinit = generatePassword();
	string password = passwordinit;
	for(int computePassword=1;computePassword<=_iteration;computePassword++){ //Nombre de fois que l'itération est faite pour réduire et hashé
		password = inSha256(password);
		password = reduce(password,computePassword);
	}
	string toFill = passwordinit + "," + password + "\n";
	information->at(index).at(indexBigList)=toFill;
}

void Rainbow::save(int ind,std::ofstream& file,std::vector<std::vector<std::string>>* information){
	/*Function writting the password and the tail in the csv file.*/
	for(int i=0 ;i< NBRTHREAD;i++){
		file<<information->at(ind).at(i);
	}
}

void Rainbow::generateTable(){
	/*It generate the Rainbow table*/
	//List where the head and the tail will be store.
	std::vector<std::vector<std::string>>* information = new std::vector<std::vector<std::string>>();
	for(int z=0;z<2;z++){
		std::vector<std::string> temp;
		for(int i =0;i<NBRTHREAD;i++){
			temp.push_back(std::string());
		}
		information->push_back(temp);
	}
	std::vector<std::thread> all; // Threads used for the hashing and reducing process
	//Create file where the informations will be stored.
	std::ofstream file;
	file.open(_fileName);
	file<<"head,tail\n";
	
	std::thread* saver; //Thread with the responsability to write in the file.
	for(int z=0;z<GENERATEPASSWORD/NBRTHREAD;z++){
		if(z>=1){
			saver->join();
			delete saver;
		}
		for(int i=0;i<NBRTHREAD;i++){
			all.push_back(std::thread(&Rainbow::reduceAndHashingProcess,this,i,z%2,information));
		}
		while(all.size()!=0){
			all.at(0).join();
			all.erase(all.begin());
		}
		saver=new std::thread(&Rainbow::save,this,z%2,std::ref(file),information);
	}
	saver->join();
	delete saver;
	file.close();
}

string Rainbow::inSha256(string word){
	/*hashing a word.*/
	return sha256(word);
}


string Rainbow::generatePassword(){
	/*Generation of random password*/
	string password = "";
	//Generation of random number
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist6(0,35);

	int genNumber;
	for(int i=0;i<_wordlength;i++){
		genNumber = int(dist6(rng));//random number
		password+=_allLetters[genNumber];
	}
	return password;
}

void Rainbow::hexToDeci(const char *text, unsigned char* bytes)
{
	/*Function converting the hash which is in hexadecimal into an array of int*/
    unsigned int temp;
    for(int i = 0; i < 16; i++) {
        sscanf( text + 2 * i, "%2x", &temp );
        bytes[i] = temp;
    }
}


string Rainbow::reduce(string hash,int iter){
	/*Function corresponding to the reduction of a certain hash at a certain iteration*/
	int index;
	string reduce = "";
	unsigned char* bytes = new unsigned char[16];
	hexToDeci(hash.c_str(), bytes);
	for(int letter=0;letter<_wordlength;letter++){
		index = bytes[(letter+iter) % 16];

		reduce += _allLetters[index % _allLetters.length()];
	}

	return reduce;
}

void Rainbow::attack(int AttackPassword){
	/*Function used to attack the Rainbow table.*/
	string randomHash;
	int step;
	string headFound;
	string foundPassword;
	for(int nbrOfAttack=0;nbrOfAttack<AttackPassword;nbrOfAttack++){
		randomHash=inSha256(generatePassword());
		step = _iteration ;
		headFound = compareTail(reduce(randomHash,step));
		if(not headFound.empty()){//if a solution is found after the first reduction.
			foundPassword = buildUp(headFound,randomHash);
			if(foundPassword!=""){
				std::cout<<"Le mot de passe trouvé est: "<<foundPassword<<std::endl;
			}
			else{
				std::cout<<"L'attaque "<<nbrOfAttack<<" a échoué!"<<std::endl;
			}
		}
		else{
			string process = randomHash;
			step-=1;
			bool tailFound = false;
			while(step>=1 and not tailFound){
				process = reduce(process,step);
				for(int i=step+1;i<=_iteration;i++){
					process = inSha256(process);
					process = reduce(process,i);
				}
				headFound = compareTail(process);
				if(not headFound.empty()){
					tailFound = true;
					string foundPassword = buildUp(headFound,randomHash);
					if(foundPassword!=""){
						std::cout<<"L'attaque "<<nbrOfAttack<<" a réussi ! Le mot de passe trouvé est: "<<headFound<<std::endl;
					}
					else{
						std::cout<<"L'attaque "<<nbrOfAttack<<" a échoué!"<<std::endl;
					}
				}
				process = randomHash;//Start again with the initial hash.
				step-=1;
			}
		}
	}
}



string Rainbow::compareTail(string lastReduction){
	/*Compare if the parameter 'lastReduction' is equal to any password in the csv file*/
	std::ifstream csvFile;
	csvFile.open(_fileName);
	string tempHead="";
	string line;
	while(csvFile.good()){
		std::vector<string> results;
		getline(csvFile,line,'\n');
		if (line.size()!=0){
			boost::split(results,line, boost::is_any_of(","));
			if((lastReduction).compare(results[1])==0){
				tempHead+=results[0];
				csvFile.close();
				return tempHead;
			}
		}	
	}
	tempHead="";
	csvFile.close();
	return tempHead;
}

string Rainbow::buildUp(string head, string randomHash){
	/*Start from the head and try to find the parameter "randomHash" by processing the Hashing and reduction*/
	string hash;
	string reduced;
	string foundPassword = "";
	reduced = head;
	for(int rainbowIteration=1;rainbowIteration<=_iteration;rainbowIteration++){
		hash = inSha256(reduced);
		if((hash).compare(randomHash)==0){
			foundPassword+=reduced;
			return foundPassword;
		}
		reduced = reduce(hash,rainbowIteration);
		
	}
	return foundPassword;
}