#ifndef RAINBOW_H
#define RAINBOW_H
#include <iostream>
#include "sha256.h"
#include <fstream>
#include <vector>
#define REDUCTIONS 50000 //Number of hashing and reductions.
#define ALLKEY "abcdefghijklmnopqrstuvwxyz123456789"//every possible characters.
#define GENERATEPASSWORD 1000000 //Number of password generated.
						 //1000000  //for 125MB
						 //10000000 //for 1GB
#define NBRTHREAD 20 //Number of threads used.

using std::string;


class Rainbow {
private:
	int _iteration;
	int _wordlength;
	string _allLetters;
	string _fileName;

public:
	Rainbow(int,string,bool);
	void generateTable();
	string reduce(string,int);
	string inSha256(string);
	string generatePassword();
	void attack(int);
	string compareTail(string);
	string buildUp(string,string);
	void hexToDeci(const char*, unsigned char*);
	void reduceAndHashingProcess(int,int,std::vector<std::vector<string>>*);
	void save(int,std::ofstream&,std::vector<std::vector<string>>*);
};

#endif

